var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: {type: '2dsphere', sparse: true}
    }
})

bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    })
}

bicicletaSchema.methods.toString = function(){
    return 'code: ' + this.code + '|color: ' + this.color;
}

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({},cb);
}

bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici, cb);
}
bicicletaSchema.statics.findById = function(id,cb){
    return this.findOne({_id:id},cb);
}
bicicletaSchema.statics.findByCode = function(aCode, cb){
    return this.findOne({code: aCode}, cb);
}

bicicletaSchema.statics.updateById = function(id,nCode,nColor,nModel,nLat,nLng,cb){
    var consulta = {_id: id};
    this.update(consulta,{$set:{
            code:nCode,
            color:nColor,
            modelo:nModel,
            ubicacion:
               [nLat,nLng]
        }
    },cb);
}

bicicletaSchema.statics.updateByCode = function(aCode,nCode,nColor,nModel,nLat,nLng,cb){
    var consulta = {code: aCode};
    this.update(consulta,{$set:{
            code:nCode,
            color:nColor,
            modelo:nModel,
            ubicacion:
               [nLat,nLng]
        }
    },cb);
}

bicicletaSchema.statics.removeByCode = function(aCode, cb){
    return this.deleteOne({code: aCode}, cb);
}

bicicletaSchema.statics.removeById = function(id,cb){
    this.deleteOne({_id:id},cb);
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

/*
var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString =function(){
    return 'id: ' + this.id + '|color: ' + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function (aBici) {
    Bicicleta.allBicis.push(aBici);   
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x=>x.id == aBiciId)
    if(aBici)
        return aBici
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);    
}

Bicicleta.removeById = function (aBiciId){
    for(var i=0; i< Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}
/*
var a = new Bicicleta(1,"negro",'urbano',[-6.878180, -79.919315]);
var b = new Bicicleta(2,'rojo','montañera',[-6.877530, -79.919809]);

var a = new Bicicleta(1,'negro','urbano',[-6.877565, -79.919793]);
var b = new Bicicleta(2,'rojo','montañera',[-6.883738, -79.921665]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports=Bicicleta;
*/
